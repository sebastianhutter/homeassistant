#!/usr/bin/env python

"""
    set hdmi input with telnet commands
"""

import telnetlib
import argparse

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description='Set hdmi input for gefen switch.')
        parser.add_argument('--ip', required=True, type=str)
        parser.add_argument('--input', required=True, type=int)
        args = parser.parse_args()
        
        tn = telnetlib.Telnet(args.ip)
        tn.write(bytes("R {}\r\n".format(args.input), 'ascii'))
        tn.close()
    except Exception as e:
        print("Unknown error occurded: {}".format(e))