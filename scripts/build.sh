#!/bin/bash 

# build and push the docker image
TAG="stable"

echo build custom homeassistant image based on "${TAG}"
docker build --build-arg version=${TAG} -t sebastianhutter/home-assistant:${TAG} .
docker push sebastianhutter/home-assistant:${TAG} 