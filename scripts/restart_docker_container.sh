#!/bin/bash

#
# restart a docker container
# arg1 = node
# arg2 = container

# ssh configuration
SSH_USER="rancher"
SSH_KEY="/keys/id_rsa.homeassistant"
SSH_COMMAND="sudo docker restart"

# get command line parameters
NODE="$1"
CONTAINER="$2"
[ -z "${NODE}" ] && echo "no node specified. aborting" && exit 1
[ -z "${CONTAINER}" ] && echo "no container specified. aborting" && exit 1

# check if ssh key exists
[ ! -f "${SSH_KEY}" ] && echo "ssh key file '${SSH_KEY}' not found. aborting" && exit 1

# connect to the node and restart
echo "Restart container '${CONTAINER}' on node '$NODE'"
ssh -i "${SSH_KEY}" -l "${SSH_USER}" -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" ${NODE} ${SSH_COMMAND} ${CONTAINER} >/dev/null 2>&1

if [ "$?" -ne 0 ]; then
    echo "Unable to restart container. Container name correct?"
    exit 1
fi 