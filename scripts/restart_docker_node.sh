#!/bin/bash

#
# restart a docker node
# arg1 = node

# ssh configuration
SSH_USER="rancher"
SSH_KEY="/keys/id_rsa.homeassistant"
SSH_COMMAND="sudo reboot"

# get command line parameters
NODE="$1"
[ -z "${NODE}" ] && echo "no node specified. aborting" && exit 1

# check if ssh key exists
[ ! -f "${SSH_KEY}" ] && echo "ssh key file '${SSH_KEY}' not found. aborting" && exit 1

# connect to the node and restart
echo "Restart node '$NODE'"
ssh -i "${SSH_KEY}" -l "${SSH_USER}" -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" ${NODE} ${SSH_COMMAND} ${CONTAINER} >/dev/null 2>&1

if [ "$?" -ne 0 ]; then
    echo "Unable to restart node."
    exit 1
fi 