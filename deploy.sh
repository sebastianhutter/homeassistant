#!/bin/bash

#
# copy configuration files to nas
# and restart homeassistant
#

##
# VARS
##
FILES="configuration.yaml customize.yaml automations.yaml groups.yaml scripts.yaml ui-lovelace.yaml alexa_confirm.yaml intent_script.yaml"
CONFIG_REMOTE=nas:/volume1/docker/homeassistant/config

CONTAINER_HOST=node01
CONTAINER_NAME=hoss_homeassistant_1
##
# MAIN
##

echo copy configuration files
for f in ${FILES}; do
scp ${f} ${CONFIG_REMOTE}/${f}
done

echo get homeassistant container id
container_id=$(ssh ${CONTAINER_HOST} "sudo bash -c \"docker ps | grep ${CONTAINER_NAME} | cut -d' ' -f1\"" 2>/dev/null)
[ -z "${container_id}" ] && "couldnt retrieve container id for ${CONTAINER_NAME}. aborting" && exit 1

echo restart container ${container_id}
ssh ${CONTAINER_HOST} "sudo docker restart ${container_id}"

echo "get logs for container ${container_id}"
ssh ${CONTAINER_HOST} "sudo docker logs --tail=5 -f ${container_id}"